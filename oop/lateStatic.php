<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
           class abc
           {
               protected static $name='abc';
               public function getName()
               {
                   echo "this is name of class ". static::$abc;
                   echo '<br>';
               }
           }
           class xyz extends abc
           {
               protected static $name="xyz";
           
           }
           $abc=new abc();
           $xyz=new xyz();
           $abc->getName();
           $xyz->getName();
        ?>
    </body>
</html>
