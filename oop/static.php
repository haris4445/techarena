<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        class House
        {
            public static $name='xyz';
            public static $objectCount=0;
            public static function setname($n)
            {
                self::$name=$n;
        }

            public static function get_name()
            {
                return self::$name;
            }
            public static function get_object_count()
            {
                return self::$objectCount;
            }
            public function __construct() 
            {
                self::$objectCount++;
            }
        }
        $a=new House();
        $b=new House();
        $c=new House();
        $d=new House();
        House::setname('abc<br>');
        echo  House::get_name();
        echo House::get_object_count();
        ?>
    </body>
</html>
