<?php
abstract class BaseEmployee
{   protected $FirstName;
	protected $LastName;
	
	public function getFullName()
	{
		return $this->FirstName .''. $this->LastName;
	}
	public abstract function getMonthlySalary();
	public function __construct($f,$l)
	{
		$this->FirstName=$f;
		$this->LastName=$l;
	}
	
}

class FullTimeEmploye extends BaseEmployee
{
	
	protected $AnnualSalary;
	
	
	public function getMonthlySalary()
	{
		return $this->AnnualSalary/12;
	}
	
}

class ContractEmployee extends BaseEmployee
{
	protected $HourlyRate=30;
	protected $TotalHours=80;
	
	public function getMonthlySalary()
	{
		return $this->HourlyRate * $this->TotalHours;
	}
	
}

$fulltime=new FullTimeEmploye('FullTime','Employee');
$contract=new ContractEmployee('FullTime','Employee');

echo $fulltime->getFullName();
echo $contract->getFullName();

echo $fulltime->getMonthlySalary();