<?php
class FullTimeEmploye
{
	protected $FirstName;
	protected $LastName;
	protected $AnnualSalary;
	public function __construct($f,$l)
	{
		$this->FirstName=$f;
		$this->LastName=$l;
	}
	public function getFullName()
	{
		return $this->FirstName .''. $this->LastName;
	}
	public function getAnnualSalary()
	{
		return $this->AnnualSalary/12;
	}
	
}

class ContractEmployee
{
	protected $FirstName;
	protected $LastName;
	protected $HourlyRate;
	protected $TotalHours;
	
	public function getFullName()
	{
		return $this->FirstName .''. $this->LastName;
	}
	public function getAnnualSalary()
	{
		return $this->HourlyRate * $this->TotalHours;
	}
	public function __construct($f,$l)
	{
		$this->FirstName=$f;
		$this->LastName=$l;
	}
}

$fulltime=new FullTimeEmploye('FullTime','Employee');
$contract=new ContractEmployee('Part time ','Worker');

echo $fulltime->getFullName();
echo "<br>";
echo $contract->getFullName();