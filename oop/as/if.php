<?php

interface abc 
{
	
	public function test();            //function abstract hon gy
	
	public function xyz();            //hum privat ya protected function ya variable define ni kr skty
}                                     //hum is me constructor funnction declare ni kr skty

interface abcd
{
	public function test2();


}

class def implements abc,abcd          //hum 1 sy zada interface implement lr sakty hn, lkn 1 sy zada classes ni 
{
	public function test()
	{
		echo "Test Function";
	}
	
	public function xyz()
	{
		echo "XYZ Function ";
	}
	
	public function test2()
	{
		echo "This is function 2";
	}
}


?>