<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        abstract class shape
        {
            public abstract function draw();
        }
        class rect extends shape
        {
            public function draw() {
                echo 'this is rectangular<br/>';
            }
        }
        class oval extends shape
        {
            public function draw() {
                echo 'this is oval<br/>';
            }
        }
        class circle extends shape
        {
            public function draw()
            {
                echo 'this is circle <br />';
            }
        }
        $s=new rect();
        $s->draw();
        $o=new oval();
        $o->draw();
		$c =new circle();
		$c->draw();
        ?>
    </body>
</html>
